package config

type Config struct {
	Name        *string      `mapstructure:"name" yaml:"name"`
	Database    *Database    `mapstructure:"database" yaml:"database"`
	Redis       *Redis       `mapstructure:"redis" yaml:"redis"`
	Bot         *Bot         `mapstructure:"bot" yaml:"bot"`
	S3          *S3          `mapstructure:"s3" yaml:"s3"`
	IPDB        *IPDB        `mapstructure:"ipdb" yaml:"ipdb"`
	GRPC        *GRPC        `mapstructure:"grpc" yaml:"grpc"`
	GRPCClients *GRPCClients `mapstructure:"grpc_clients" yaml:"grpc_clients"`
	GRPCClient  *GRPCClient  `mapstructure:"grpc_client" yaml:"grpc_client"`
	Telegraph   *Telegraph   `mapstructure:"telegraph" yaml:"telegraph"`
	Listen      *Listen      `mapstructure:"listen" yaml:"listen"`
	S3Storage   *S3Storage   `mapstructure:"s3storage" yaml:"s3storage"`
	Proxies     *Proxies     `mapstructure:"proxies" yaml:"proxies"`
	Server      *Server      `mapstructure:"server" yaml:"server"`
	MongoDB     *MongoDB     `mapstructure:"mongodb" yaml:"mongodb"`
	API         *API         `mapstructure:"api" yaml:"api"`
	Mikrotik    *Mikrotik    `mapstructure:"mikrotik" yaml:"mikrotik"`
	// K8S struct {
	// 	Config     *string  `mapstructure:"config" yaml:"config" help:"путь до конфига доступа к k8s"`
	// 	Namespases []string `mapstructure:"namespaces" yaml:"namespaces" help:"список неймспейсов k8s"`
	// } `mapstructure:"k8s" yaml:"k8s" help:"конфиг k8s"`
	// Bots     struct {
	// 	Authorizer *Bot `mapstructure:"authorizer" yaml:"authorizer"`
	// 	Autohelper *Bot `mapstructure:"autohelper" yaml:"autohelper"`
	// } `mapstructure:"bots" yaml:"bots"`
	// TlgClient *TlgClient `mapstructure:"tlgclient" yaml:"tlgclient"`
	// Listen    *Listen `mapstructure:"listen" yaml:"listen"`
	// UploadURL string  `mapstructure:"upload_url" yaml:"upload_url"`
}

type API struct {
	TLS     *TLS    `mapstructure:"tls" yaml:"tls"`
	Address string  `mapstructure:"address" yaml:"address"`
	Port    string  `mapstructure:"port" yaml:"port"`
}

type Mikrotik struct {
	Address  string `mapstructure:"address" yaml:"address"`
	Port  string `mapstructure:"port" yaml:"port"`
	Username string `mapstructure:"username" yaml:"username"`
	Password string `mapstructure:"password" yaml:"password"`
	TLS     *TLS    `mapstructure:"tls" yaml:"tls"`
}

type Proxies struct {
	File *struct {
		Bucket string `mapstructure:"bucket" yaml:"bucket"`
		Path   string `mapstructure:"path" yaml:"path"`
		Name   string `mapstructure:"name" yaml:"name"`
	} `mapstructure:"file" yaml:"file"`
	OnePass string   `mapstructure:"one_pass" yaml:"one_pass"`
	Sheme   string   `mapstructure:"sheme" yaml:"sheme"`
	Port    string   `mapstructure:"port" yaml:"port"`
	URL     string   `mapstructure:"url" yaml:"url"`
	Proxy   []string `mapstructure:"proxy" yaml:"proxy"`
}

type Listen struct {
	TLS     *TLS    `mapstructure:"tls" yaml:"tls"`
	Address string  `mapstructure:"address" yaml:"address"`
	Port    string  `mapstructure:"port" yaml:"port"`
	Limits  *Limits `mapstructure:"limits" yaml:"limits"`
}

type Limits struct {
	PerSecond int `mapstructure:"second" yaml:"second"`
	PerDay    int `mapstructure:"day" yaml:"day"`
}

type TLS struct {
	CA   string `mapstructure:"ca" yaml:"ca"`     // base64
	Cert string `mapstructure:"cert" yaml:"cert"` // base64
	Key  string `mapstructure:"key" yaml:"key"`   // base64
}

type S3 struct {
	Endpoint  string `mapstructure:"endpoint" yaml:"endpoint"`
	AccessKey string `mapstructure:"access_key" yaml:"access_key"`
	SecretKey string `mapstructure:"secret_key" yaml:"secret_key"`
	Region    string `mapstructure:"region" yaml:"region"`
	Secure    bool   `mapstructure:"secure" yaml:"secure"`
}

type IPDB []IPDBInfo

// bucket     path                    name
// .-^-. .------^------. .-------------^--------------.
// geoip/maxmind/bla/bla/GeoIP2-Connection-Type-CSV.zip

type IPDBInfo struct {
	URL    *string `mapstructure:"url"`    // url to download file with ip information
	SHA    *string `mapstructure:"sha"`    // url to download file with SHA sum of URL file
	Path   string  `mapstructure:"path"`   // path without bucket name and file name (ex.: maxmind/bla/bla)
	Type   string  `mapstructure:"type"`   // geoip2|csv|iplist|ipnetlist
	Name   string  `mapstructure:"name"`   // name of file to save on S3
	Bucket string  `mapstructure:"bucket"` // only bucket name
}

type Server struct {
	RPC string `mapstructure:"rpc" yaml:"rpc"`
	Web string `mapstructure:"web" yaml:"web"`
	Crt string `mapstructure:"crt" yaml:"crt"`
	Key string `mapstructure:"key" yaml:"key"`
	CA  string `mapstructure:"ca" yaml:"ca"`
}

type MongoDB struct {
	URL      string `mapstructure:"url" yaml:"url"`
	Database string `mapstructure:"database" yaml:"database"`
} 

type Database struct {
	DSN        string `mapstructure:"dsn" yaml:"web" help:"cтрока с адресом подключения к БД"`
	Path       string
	Username   string `mapstructure:"username" yaml:"username" help:"username доступа к БД"`
	Password   string `mapstructure:"password" yaml:"password" help:"password доступа к БД"`
	SAKey      string `mapstructure:"sakey" yaml:"sakey" help:"sakey доступа к БД"` //base64
	Single     bool   `mapstructure:"single" yaml:"single" help:"подключение к БД через ингресс"`
	DBUser     string `mapstructure:"dbuser" yaml:"dbuser" help:"username доступа к БД"`
	DBPassword string `mapstructure:"dbpassword" yaml:"dbpassword" help:"password доступа к БД"`
	DBHost     string `mapstructure:"dbhost" yaml:"dbhost" help:"host доступа к БД"`
	DBPort     string `mapstructure:"dbport" yaml:"dbport" help:"port доступа к БД"`
	DBName     string `mapstructure:"dbname" yaml:"dbname" help:"название БД"`
	DBCert     string `mapstructure:"dbcert" yaml:"dbcert" help:"сертиы=фикат для подключеения к БД в base64"`
}

type TlgClient struct {
	AppID   int    `mapstructure:"appid" yaml:"appid"`
	AppHASH string `mapstructure:"apphash" yaml:"apphash"`
}

type Bot struct {
	Token   string    `mapstructure:"token" yaml:"token"`
	Admins  []string  `mapstructure:"admins" yaml:"admins"`
	Env     string    `mapstructure:"env" yaml:"env"`
	Links   *BotLinks `mapstructure:"links" yaml:"links"`
	Webhook struct {
		CA         *string `mapstructure:"ca" yaml:"ca"`     //base64
		Cert       *string `mapstructure:"cert" yaml:"cert"` //base64
		Key        *string `mapstructure:"key" yaml:"key"`   //base64
		Addr       string  `mapstructure:"addr" yaml:"addr"`
		Port       string  `mapstructure:"port" yaml:"port"`
		ListenAddr string  `mapstructure:"listen_addr" yaml:"listen_addr"`
		ListenPort string  `mapstructure:"listen_port" yaml:"listen_port"`
	} `mapstructure:"webhook" yaml:"webhook"`
	Proxy struct {
		Type string `mapstructure:"type" yaml:"type"`
		Addr string `mapstructure:"addr" yaml:"addr"`
		Port string `mapstructure:"port" yaml:"port"`
		User string `mapstructure:"user" yaml:"user"`
		Pass string `mapstructure:"pass" yaml:"pass"`
	} `mapstructure:"proxy" yaml:"proxy"`
	S3Bucket *string   `mapstructure:"bucket" yaml:"bicket"`
	S3Path   *string   `mapstructure:"path" yaml:"path"`
	Payments *Payments `mapstructure:"payments" yaml:"payments"`
}

type BotLinks struct {
	WebApp  string `mapstructure:"webapp" yaml:"webapp"`
	Site    string `mapstructure:"site" yaml:"site"`
	Admin   string `mapstructure:"admin" yaml:"admin"`     // admin contact
	Group   string `mapstructure:"group" yaml:"group"`     //simple group link
	Support string `mapstructure:"support" yaml:"support"` //multigroup link
}

type Payments struct {
	WalletPay *WalletPay `mapstructure:"walletpay" yaml:"walletpay"`
	Price     float64    `mapstructure:"price" yaml:"price"`
}

type WalletPay struct {
	APIKey     string `mapstructure:"apikey" yaml:"apikey"`
	Addr       string `mapstructure:"addr" yaml:"addr"`
	ListenAddr string `mapstructure:"listen_addr" yaml:"listen_addr"`
	ListenPort string `mapstructure:"listen_port" yaml:"listen_port"`
}

type Redis struct {
	RedisHost     string `mapstructure:"rhost" yaml:"rhost" help:"host доступа к Redis"`
	RedisPort     string `mapstructure:"rport" yaml:"rport" help:"port доступа к Redis"`
	RedisUsername string `mapstructure:"rusername" yaml:"rusername" help:"username доступа к Redis"`
	RedisPassword string `mapstructure:"rpassword" yaml:"rpassword" help:"password доступа к Redis"`
	RedisDBName   int    `mapstructure:"rdbname" yaml:"rdbname" help:"название Redis"`
}

type GRPC struct {
	TLS     *TLS   `mapstructure:"tls" yaml:"tls"`
	Address string `mapstructure:"address" yaml:"address"`
	Port    string `mapstructure:"port" yaml:"port"`
}

type GRPCClients []GRPCClient

type GRPCClient struct {
	TLS     *TLS   `mapstructure:"tls" yaml:"tls"`
	Name    string `mapstructure:"name" yaml:"name"`
	Address string `mapstructure:"address" yaml:"address"`
	Port    string `mapstructure:"port" yaml:"port"`
}

type Telegraph struct {
	AuthorName *string `mapstructure:"author_name" yaml:"author_name"`
	ShortName  *string `mapstructure:"short_name" yaml:"short_name"`
	Token      *string `mapstructure:"token" yaml:"token"`
	Secret     string  `mapstructure:"secret" yaml:"secret"`
}

// S3Storage - plase where to save something inside bucket with certain path
type S3Storage struct {
	Bucket string `mapstructure:"bucket" yaml:"bucket"`
	Path   string `mapstructure:"path" yaml:"path"`
}

